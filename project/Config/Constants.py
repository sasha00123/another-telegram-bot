# ReQuizBot
# Made by Kirill Averyanov, Alexander Batyrgariev, Blagoi Dimitrov
import os
import urllib.parse as urlparse
from peewee import PostgresqlDatabase

urlparse.uses_netloc.append("postgres")
url = urlparse.urlparse(os.environ["DATABASE_URL"])


class Constants:
    TOKEN = "383314784:AAG2RNBq6K9XaigD9Dx3HCcIP5vEqOUWvV0"
    PORT = int(os.environ.get('PORT', '5000'))
    DATABASE = PostgresqlDatabase(
        database=url.path[1:],
        user=url.username,
        password=url.password,
        host=url.hostname,
    )
