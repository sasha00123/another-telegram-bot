# ReQuizBot
# Made by Kirill Averyanov, Alexander Batyrgariev, Blagoi Dimitrov
from Config.Constants import Constants
from BaseModel import BaseModel
from CreatorModel import Creator
import peewee


class Quiz(BaseModel):
    title = peewee.CharField()
    pin = peewee.IntegerField(unique=True)
    creator = peewee.ForeignKeyField(Creator)
