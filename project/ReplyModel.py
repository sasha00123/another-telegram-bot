# ReQuizBot
# Made by Kirill Averyanov, Alexander Batyrgariev, Blagoi Dimitrov

from Config.Constants import Constants
from BaseModel import BaseModel
from PlayerModel import Player
from VariantModel import Variant
import peewee


class Reply(BaseModel):
    nick = peewee.CharField()
    variant = peewee.ForeignKeyField(Variant)
    player = peewee.ForeignKeyField(Player)
