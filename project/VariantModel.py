# ReQuizBot
# Made by Kirill Averyanov, Alexander Batyrgariev, Blagoi Dimitrov

from Config.Constants import Constants
from BaseModel import BaseModel
import peewee
from QuestionModel import Question


class Variant(BaseModel):
    text = peewee.CharField()
    question = peewee.ForeignKeyField(Question)
    right = peewee.BooleanField()
