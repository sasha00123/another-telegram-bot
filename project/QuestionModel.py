# ReQuizBot
# Made by Kirill Averyanov, Alexander Batyrgariev, Blagoi Dimitrov
from Config.Constants import Constants
from BaseModel import BaseModel
import peewee
from QuizModel import Quiz


class Question(BaseModel):
    title = peewee.CharField()
    quiz = peewee.ForeignKeyField(Quiz)
