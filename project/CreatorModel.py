# ReQuizBot
# Made by Kirill Averyanov, Alexander Batyrgariev, Blagoi Dimitrov
from Config.Constants import Constants
from BaseModel import BaseModel
import peewee


class Creator(BaseModel):
    chat_id = peewee.IntegerField()
