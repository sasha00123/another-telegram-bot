# ReQuizBot
# Made by Kirill Averyanov, Alexander Batyrgariev, Blagoi Dimitrov
from Config.Constants import Constants
from BaseModel import BaseModel
from QuizModel import Quiz
import peewee


class Player(BaseModel):
    chat_id = peewee.IntegerField()
    quiz = peewee.ForeignKeyField(Quiz)
