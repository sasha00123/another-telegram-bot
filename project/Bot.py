# ReQuizBot
# Made by Kirill Averyanov, Alexander Batyrgariev, Blagoi Dimitrov

from Config.Constants import *
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, \
                        ConversationHandler, RegexHandler
from telegram import ReplyMarkup, ReplyKeyboardMarkup, KeyboardButton
from telegram import User
from BasicInfoClass import BasicInfo
from QuizModel import Quiz
from CreatorModel import Creator
from peewee import DoesNotExist
from QuestionModel import Question
from VariantModel import Variant
from PlayerModel import Player
from random import randint
from random import shuffle

QUIZ_NAME, ADD_QUESTION, ADD_VARIANTS, CHOOSE = range(4)
ENTERED, ASK_WITH_VARIANTS = range(2)



def start(bot, update):
    update.message.reply_text(BasicInfo.Start)


def help(bot, update):
    update.message.reply_text(BasicInfo.Help)


def enter(bot, update):
    update.message.reply_text(BasicInfo.Enter)
    return ENTERED


def entered(bot, update, user_data):
    # TODO: Check right answer
    try:
        pin = int(update.message.text)
    except:
        update.message.reply_text(BasicInfo.IncorrectAnswer)
        return ENTERED

    try:
        quiz = Quiz.get(pin=pin)

        player = Player()
        player.chat_id = update.message.chat_id
        player.quiz = quiz
        player.save()
        user_data["quiz"] = quiz
        user_data["player"] = player
        user_data["questions"] = [{"question":question, "variants":[variant for variant in Variant.filter(question=question)]} for question in Question.filter(quiz=quiz)]
        user_data["question_counter"] = 0
        user_data["score"] = 0
        update.message.reply_text(BasicInfo.EnterSucceded)
        update.message.reply_text(user_data["questions"][user_data["question_counter"]]["question"].title)
        variants_str = ""
        question = user_data["questions"][user_data["question_counter"]]["question"]
        variants = user_data["questions"][user_data["question_counter"]]["variants"]
        for var in variants:
            variants_str += "-" + var.text + "\n"

        reply_keyboard = [[BasicInfo.FirstAnswer, BasicInfo.SecondAnswer],
                          [BasicInfo.ThirdAnswer, BasicInfo.ForthAnswer]]

        update.message.reply_text(variants_str,
                          reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                           one_time_keyboard=True))
        return ASK_WITH_VARIANTS
    except Exception as e:
        update.message.reply_text("Такая викторина не нашлась) Попробуйте еще раз")
        return ENTERED


def exit(bot, update):
    update.message.reply_text(BasicInfo.Exit)
    # TODO: implement exit


def quizzes(bot, update):
    update.message.reply_text(BasicInfo.Quizzes)
    # TODO: show quizzes list


def stop(bot, update):
    update.message.reply_text(BasicInfo.Stop)
    # TODO: implement exit


def give_answer(bot, update, user_data):
    try:
        ans = {BasicInfo.FirstAnswer:0, BasicInfo.SecondAnswer:1,
         BasicInfo.ThirdAnswer:2, BasicInfo.ForthAnswer:3}
        sel = ans[update.message.text]
        if user_data["questions"][user_data["question_counter"]]["variants"][sel].right:
            user_data["score"] += 1
            update.message.reply_text(BasicInfo.RightAnswer().format(user_data["score"], "1", "1"))
        else:
            update.message.reply_text(BasicInfo.WrongAnswer().format(user_data["score"], "1", "1"))
        user_data["question_counter"] += 1
        if user_data["question_counter"] >= len(user_data["questions"]):
            update.message.reply_text(BasicInfo.QuizEndedLooser.format(user_data["score"], "1"))
            user_data.clear()
            return ConversationHandler.END
        update.message.reply_text(BasicInfo.NextQuestion)
        update.message.reply_text(user_data["questions"][user_data["question_counter"]]["question"].title)
        variants_str = ""
        question = user_data["questions"][user_data["question_counter"]]["question"]
        variants = user_data["questions"][user_data["question_counter"]]["variants"]
        shuffle(variants)
        for var in variants:
            variants_str += "-" + var.text + "\n"

        reply_keyboard = [[BasicInfo.FirstAnswer, BasicInfo.SecondAnswer],
                          [BasicInfo.ThirdAnswer, BasicInfo.ForthAnswer]]

        update.message.reply_text(variants_str,
                          reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                           one_time_keyboard=True))
        return ASK_WITH_VARIANTS
    except Exception as e:
        update.message.reply_text(repr(e))


def quizstart(bot, update):
    update.message.reply_text(BasicInfo.QuizStarts)
    update.message.reply_text(BasicInfo.QuizStarted)
    # TODO: implement quizstart


def next_question(bot, update):
    update.message.reply_text(BasicInfo.NextQuestion)
    # TODO: implement next question


def create_quiz(bot, update, user_data):
    try:
        update.message.reply_text(BasicInfo.CreateQuiz)
        try:
            creator = Creator.get(chat_id=update.message.chat_id)
        except DoesNotExist:
            creator = Creator.create(chat_id=update.message.chat_id)
        quiz = Quiz()
        quiz.creator = creator
        user_data["quiz"] = quiz
        user_data["question_counter"] = 1
        user_data["questions"] = []
        return QUIZ_NAME
    except Exception as e:
        update.message.reply_text(str(e))


def add_title(bot, update, user_data):
    try:
        user_data["quiz"].title = update.message.text
        update.message.reply_text(BasicInfo.SetQuestionText.format(user_data["question_counter"]))
        return ADD_QUESTION
    except Exception as e:
        update.message.reply_text(str(e))

def add_question(bot, update, user_data):
    try:
        question = Question()
        question.title = update.message.text
        user_data["questions"].append({
            "question": question,
            "variants": []
        })
        update.message.reply_text(BasicInfo.SetCountQuestionAnswers)
        return ADD_VARIANTS
    except Exception as e:
        update.message.reply_text(str(e))

def add_variants(bot, update, user_data):
    try:
        variants = update.message.text.split(";")
        if len(variants) != 4:
            update.message.reply_text(BasicInfo.IncorrectAnswer)
            return ADD_VARIANTS
        for var in variants:
            variant = Variant()
            variant.text = var.strip()
            user_data["questions"][-1]["variants"].append(variant)

        reply_keyboard = [[BasicInfo.AddQuestion, BasicInfo.EndCreation]]
        update.message.reply_text(BasicInfo.ChooseCreating,
                                  reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                                   one_time_keyboard=True))
        return CHOOSE
    except Exception as e:
        update.message.reply_text(str(e))

def generate_pin():
    # TODO: Check pin in database
    return randint(10000, 99999)


def choose(bot, update, user_data):
    try:
        if update.message.text == BasicInfo.AddQuestion:
            user_data["question_counter"] += 1
            update.message.reply_text(BasicInfo.SetQuestionText.format(user_data["question_counter"]))
            return ADD_QUESTION
        else:
            pin = generate_pin()
            user_data["quiz"].pin = pin
            user_data["quiz"].save()
            for q in user_data["questions"]:
                q["question"].quiz = user_data["quiz"]
                q["question"].save()
                Iter = True
                for variant in q["variants"]:
                    if Iter:
                        variant.right = True
                        Iter = False
                    else:
                        variant.right = False
                    variant.question = q["question"]
                    variant.save()
            update.message.reply_text(BasicInfo.EndCreatingQuiz.format(pin))
            return ConversationHandler.END
    except Exception as e:
        update.message.reply_text(str(e))


def cancel(bot, update):
    update.message.reply_text(BasicInfo.Cancel)
    return ConversationHandler.END


updater = Updater(Constants.TOKEN)
dp = updater.dispatcher
dp.add_handler(CommandHandler("start", start))
dp.add_handler(CommandHandler("help", help))
# dp.add_handler(CommandHandler("enter", enter))
# dp.add_handler(CommandHandler("exit", exit))
# dp.add_handler(CommandHandler("quizzes", quizzes))
# dp.add_handler(CommandHandler("stop", stop))
# dp.add_handler(CommandHandler("quizstart", quizstart))
# dp.add_handler(CommandHandler("next_question", next_question))
# dp.add_handler(CommandHandler("create_quiz", create_quiz))
create_handler = ConversationHandler(
    entry_points=[CommandHandler('create_quiz', create_quiz, pass_user_data=True)],

    states={
        QUIZ_NAME: [MessageHandler(Filters.text, add_title, pass_user_data=True)],

        ADD_QUESTION: [MessageHandler(Filters.text, add_question, pass_user_data=True)],

        ADD_VARIANTS: [MessageHandler(Filters.text, add_variants, pass_user_data=True)],

        CHOOSE: [RegexHandler('^({}|{})$'.format(BasicInfo.AddQuestion, BasicInfo.EndCreation),
                              choose, pass_user_data=True)],
    },

    fallbacks=[CommandHandler('cancel', cancel)]
)

play_handler = ConversationHandler(
    entry_points=[CommandHandler(
        'enter', enter)],

    states={
        ENTERED: [MessageHandler(Filters.text, entered, pass_user_data=True)],

        ASK_WITH_VARIANTS: [RegexHandler('^({}|{}|{}|{})$'.format(BasicInfo.FirstAnswer,
                                                                  BasicInfo.SecondAnswer,
                                                                  BasicInfo.ThirdAnswer,
                                                                  BasicInfo.ForthAnswer),
                                         give_answer, pass_user_data=True)],
    },

    fallbacks=[CommandHandler('cancel', cancel)]
)

dp.add_handler(create_handler)
dp.add_handler(play_handler)
updater.start_webhook(listen='0.0.0.0',
                      port=Constants.PORT,
                      url_path=Constants.TOKEN)
updater.bot.set_webhook("https://requiz-bot.herokuapp.com/" + Constants.TOKEN)
updater.idle()
