# ReQuizBot
# Made by Kirill Averyanov, Alexander Batyrgariev, Blagoi Dimitrov

from peewee import Model
from Config.Constants import Constants


class BaseModel(Model):
    class Meta:
        database = Constants.DATABASE
